package com.durkeeinc.nonogram;

public class NonoGram {
    private String catalogue;
    private String title;
    private String author;
    private String width;
    private String height;
    private String[] columns;
    private String[] rows;
    private String answer;

    public NonoGram() {
        //future constructor
    }

    //Getters & Setters

    public String getCatalogue() {
        return catalogue;
    }
    
    public void setCatalogue(String catalogue) {
        this.catalogue = catalogue;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String[] getRows() {
        return rows;
    }

    public void setRows(String[] rows) {
        this.rows = rows;
    }

    public String[] getColumns() {
        return columns;
    }

    public void setColumns(String[] columns) {
        this.columns = columns;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
