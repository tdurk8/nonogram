package com.durkeeinc.nonogram;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.springframework.core.io.ResourceLoader;

@SpringBootApplication
public class NonogramApplication implements CommandLineRunner {
	
	@Autowired
	ResourceLoader resourceLoader;
	public static void main(String[] args) {
		SpringApplication.run(NonogramApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		NonogramFile nonoGram = new NonogramFile();
	}
}
