package com.durkeeinc.nonogram;

import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class NonogramFile {
    private String nonogramFileName ="Ubuntu.non";
    private String defaultPath ="\\src\\main\\resources\\nonograms\\";
    private String[][] dataFields = {{"catalogue",""},
                            {"title",""},
                            {"by",""},
                            {"width",""},
                            {"height",""},
                            {"goal",""},
                            {"copyright",""},
                            {"license",""}};
    
    public NonogramFile()throws IOException{
        loadNonogramFile(defaultPath,nonogramFileName);
    }
    public NonogramFile(String filePath, String fileName)throws IOException{
        loadNonogramFile(filePath, fileName);
    }

    public void loadNonogramFile(String filePath, String fileName)throws IOException{

        File file = null;
        file = ResourceUtils.getFile(filePath + fileName);
        if (file.length()==0)
            System.out.println("File is empty");
        else{
            parseNonoGram(file);
        } 
    }

    public void parseNonoGram(File file){
        // 2d array for category and values
        int index=0;
        String nextLine ="";
        
        //For loop will locate each string in text file
        //columns and rows will be handled seperately
        for(String[] dataField : dataFields){
                try(Scanner in = new Scanner(file)){
                    while(in.hasNextLine()){
                        nextLine=in.nextLine();
                        if(nextLine.contains(dataField[0])) {
                            dataFields[index][1]=cleanStrings(nextLine,dataField[0]);
                            break;
                        }
                    }
                }catch(Exception e){
                    System.out.print("Invalid File Format");
                }
            index++;
        }
        printGoal();
    }

    private String cleanStrings(String dirtyString, String category){
        String cleanString="";
        cleanString= dirtyString.replace(category, "");
        cleanString= cleanString.replace("\"","").trim();
        System.out.println(cleanString);

        return cleanString;
    }

    public void printGoal(){
        //display the correct puzzle solution
        int cols = Integer.valueOf(dataFields[3][1]);
        int rows = Integer.valueOf(dataFields[4][1]);
        int charIndex =0;
        String goal = dataFields[5][1];

        for (int x=0; x < rows;x++){ 
            if (x==0){
            }
            for( int y=0; y < cols; y++){ 
                if (goal.charAt(charIndex)==(char)'1'){
                    System.out.print('X');
                }
                else{
                    System.out.print(" ");
                }
                charIndex++;
            }
            System.out.println();
        }
    }
    
}