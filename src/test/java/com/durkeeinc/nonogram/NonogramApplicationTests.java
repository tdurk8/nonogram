package com.durkeeinc.nonogram;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import net.bytebuddy.implementation.bind.annotation.IgnoreForBinding;

@SpringBootTest
class NonogramApplicationTests {
	private static NonogramFile loadNonogram; 
	private String filePath = "classpath:\\nonograms\\";

	@BeforeAll
	public static void init(){
		try{
			loadNonogram = new NonogramFile();
			}catch(Exception e){
				System.out.println(e);
			}
	}

	@BeforeEach
	public void initEachTest(){

	}


	// @Test
	// void contextLoads() {
	// }

	
	// @Test
	// public void LoadNonogram_FileNotFound_ThrowsFileNotFoundException(){
	// 	//Arrange
	// 	//Create Random File name to reduce chance of legit file existing
	// 	byte[] array = new byte[7]; // length is bounded by 7
	// 	new Random().nextBytes(array);
	// 	String missingFile = new String(array, Charset.forName("UTF-8"));
		
	// 	//Act--None	

	// 	//Assert
	// 	assertThrows(FileNotFoundException.class,()->{
	// 		loadNonogram.loadNonogramFile(filePath, missingFile);
	// 	});
	// }

}
