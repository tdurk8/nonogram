FROM node:8.9-alpine
ENV NODE_ENV=PROD
WORKDIR /usr/src/nonogram
COPY . /usr/src/nonograms
